package com.BookMyShow.ListMovies.parsers;

import com.BookMyShow.ListMovies.models.Movie;
import org.springframework.stereotype.Service;

import java.util.List;

@Service
public class MovieService {

    public static List<Movie> fetchAll() {
        return MovieRepository.fetchAll();
    }

}
