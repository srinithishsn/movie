package com.BookMyShow.ListMovies.parsers;

import com.BookMyShow.ListMovies.models.Movie;
import org.junit.jupiter.api.Test;
import org.junit.runner.RunWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.junit.MockitoJUnitRunner;

import java.util.ArrayList;
import java.util.List;

import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.mockito.Mockito.*;

@RunWith(MockitoJUnitRunner.class)
public class MovieServiceTest {

    @InjectMocks
    MovieService movieService;

    @Mock
    MovieRepository movieRepository;

    @Test
    void shouldReturnMovies() {
        List<Movie> movies = new ArrayList<>();

        Movie movie1 = new Movie("Tom and jerry","Infamous frenemies Tom and Jerry move to the city to start life anew. When Jerry moves into New York`s finest hotel, the event manager Kayla teams up with Tom to evict the mouse so that the `wedding of the century` can go off without a hitch. The result is one of the most elaborate cat-and-mouse games ever!","Action,Comedy,Animation","U","1hr 41m");
        Movie movie2 = new Movie("Zack Synder's Justice League","Determined to ensure Superman`s ultimate sacrifice was not in vain, Bruce Wayne aligns forces with Diana Prince to recruit a team of metahumans to protect the world.","Action,Adventure,Fantasy","UA","4hr 1m");
        movies.add(movie1);
        movies.add(movie2);

        when(movieRepository.fetchAll()).thenReturn(movies);

        List<Movie> movieList = movieService.fetchAll();
        assertEquals(2,movieList.size());
    }
}